package mk.ukim.finki.wp.jan2022.g2.service;

import mk.ukim.finki.wp.jan2022.g2.model.Discussion;
import mk.ukim.finki.wp.jan2022.g2.model.DiscussionTag;
import mk.ukim.finki.wp.jan2022.g2.model.User;
import mk.ukim.finki.wp.jan2022.g2.model.exceptions.InvalidDiscussionIdException;
import mk.ukim.finki.wp.jan2022.g2.model.exceptions.InvalidUserIdException;
import mk.ukim.finki.wp.jan2022.g2.repository.DiscussionRepository;
import mk.ukim.finki.wp.jan2022.g2.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class DiscussionServiceImpl implements DiscussionService{

    private final DiscussionRepository discussionRepository;
    private final UserRepository userRepository;

    public DiscussionServiceImpl(DiscussionRepository discussionRepository, UserRepository userRepository) {
        this.discussionRepository = discussionRepository;
        this.userRepository = userRepository;
    }

    /**
     * @return List of all entities in the database
     */
   public List<Discussion> listAll()
   {
       return discussionRepository.findAll();
   }

    /**
     * returns the entity with the given id
     *
     * @param id The id of the entity that we want to obtain
     * @return
     * @throws InvalidDiscussionIdException when there is no entity with the given id
     */
    public Discussion findById(Long id)
    {
        return discussionRepository.findById(id).orElseThrow(()->new InvalidDiscussionIdException());
    }

    /**
     * This method is used to create a new entity, and save it in the database.
     *
     * @return The entity that is created. The id should be generated when the entity is created.
     * @throws InvalidUserIdException when there is no user with the given id
     */
    public Discussion create(String title, String description, DiscussionTag discussionTag, List<Long> participants, LocalDate dueDate)
    {
        List<User> users=userRepository.findAllById(participants);
        if(users.size()==0)
            throw new InvalidUserIdException();
        Discussion discussion=new Discussion(title, description, discussionTag, users, dueDate);
      return   discussionRepository.save(discussion);

    }

    /**
     * This method is used to modify an entity, and save it in the database.
     *
     * @param id          The id of the entity that is being edited
     * @return The entity that is updated.
     * @throws InvalidDiscussionIdException when there is no entity with the given id
     * @throws InvalidUserIdException    when there is no user with the given id
     */
    public Discussion update(Long id, String title, String description, DiscussionTag discussionTag,
                             List<Long> participants)
    {
        Discussion discussion=this.findById(id);
        List<User> users=userRepository.findAllById(participants);
        if(users.size()==0)
            throw new InvalidUserIdException();
        discussion.setTitle(title);
        discussion.setDescription(description);
        discussion.setTag(discussionTag);
        discussion.setParticipants(users);
        return   discussionRepository.save(discussion);
    }

    /**
     * Method that should delete an entity. If the id is invalid, it should throw InvalidDiscussionIdException.
     *
     * @param id
     * @return The entity that is deleted.
     * @throws InvalidDiscussionIdException when there is no entity with the given id
     */
    public Discussion delete(Long id)
    {
        Discussion discussion=this.findById(id);
        discussionRepository.delete(discussion);
        return discussion;
    }

    /**
     * Method that should make an entity marked as popular. If the id is invalid, it should throw InvalidDiscussionIdException.
     *
     * @param id
     * @return The entity that should be marked as popular.
     * @throws InvalidDiscussionIdException when there is no entity with the given id
     */
    public Discussion markPopular(Long id)
    {
        Discussion discussion=this.findById(id);
        discussion.setPopular(true);
        discussionRepository.save(discussion);
        return discussion;
    }

    /**
     * The implementation of this method should use repository implementation for the filtering.
     * All arguments are nullable. When an argument is null, we should not filter by that attribute
     *
     * @return The entities that meet the filtering criteria
     */
    public List<Discussion> filter(Long participantId, Integer daysUntilClosing)
    {
        if(participantId!=null && daysUntilClosing!=null)
        {
            User user=userRepository.getById(participantId);
            return discussionRepository.findByParticipantsContainingAndDueDateBefore(user,LocalDate.now().plusDays(daysUntilClosing));
        }
        else if(participantId!=null && daysUntilClosing==null)
        {
            User user=userRepository.getById(participantId);
            return discussionRepository.findByParticipantsContaining(user);
        }
        else if(participantId==null && daysUntilClosing!=null)
        {
            return discussionRepository.findByDueDateBefore(LocalDate.now().plusDays(daysUntilClosing));
        }
        else {
            return discussionRepository.findAll();
        }
    }
}
