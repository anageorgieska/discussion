package mk.ukim.finki.wp.jan2022.g2.service;

import mk.ukim.finki.wp.jan2022.g2.model.User;
import mk.ukim.finki.wp.jan2022.g2.model.exceptions.InvalidUserIdException;
import mk.ukim.finki.wp.jan2022.g2.repository.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * returns the entity with the given id
     *
     * @param id The id of the entity that we want to obtain
     * @return
     * @throws InvalidUserIdException when there is no entity with the given id
     */
   public User findById(Long id)
   {
       return userRepository.findById(id).orElseThrow(()->new InvalidUserIdException());
   }

    /**
     * @return List of all entities in the database
     */
    public List<User> listAll()
    {
        return userRepository.findAll();
    }

    /**
     * This method is used to create a new entity, and save it in the database.
     *
     * @param username
     * @return The entity that is created. The id should be generated when the entity is created.
     */
    public User create(String username, String password, String role)
    {
        User user=new User(username, passwordEncoder.encode(password), role);
        return userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user= userRepository.findByUsername(username);
        if(user!=null)
        {
            UserDetails userDetails=new org.springframework.security.core.userdetails.User(user.getUsername(),
                    user.getPassword(), Stream.of(new SimpleGrantedAuthority(user.getRole())).collect(Collectors.toList()));
            return userDetails;

        }
        else {
            UserDetails userDetails=new org.springframework.security.core.userdetails.User(username, null, Stream.of(new SimpleGrantedAuthority(null)).collect(Collectors.toList()));
            return userDetails;
        }
    }
}
